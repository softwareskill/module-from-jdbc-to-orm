# README #

Aplikacje demo SoftwareSkill dla modułu obsługi baz danych - częśc dotycząca prostych popularnych frameworków takich jak MyBatis oraz JdbcTemplate bedących frameworkami typu Persistence Framework.

### Po co to repozytorium? ###

Repozytorium prezentuje sposób konfiguracji i wykorzystania API danego frameworka  
Struktura modułów:
- mybatis-nospring - dla przedstawienia korzystania z MyBatis bez Spring (konfiguracja poprzez pliki i/lub anotacje MyBatis)
- mybatis-spring - dla przedstawienia korzystania z MyBatis z wykorzystaniem Spring
- jdbctemplate - dla przedstawienia korzystania z JdbcTemplate w ramach Spring

* Wersja 1.0

### Opis ###
* Funkcjonalność - aplikacje w ramach modułów realizują tą samą kogikę, operującą na danych karty lub uzytkownika. Funkcjonalnośc opisana javadoc w nagłówkach klas.
* Baza danych - baza danych zawierająca informacje o kartach i użytkownikach. Wykorzystana baza H2 w trybie in memory lub PostgreSQL. Zobacz pliki sql w modulach
* Konfiguracja - nie ma konieczności konfiguracji. Wykorzystywana jest baza H2 w trybie in memory lub PostgrteSQL. Zobacz pliki w katalogu src/main/resources.   
* Zależności -  H2 (baza danych in memory) lub PostgreSQL, Mockito, JUnit 5, Spring Boot, Logback (framework do logów aplikacyjnych), MyBatis oraz spring-boot-starter-jdbc.
* Jak uruchomić testy - polecenie mvn test lub uruchomić z poziomu IDE (np. Run All tests w IntelliJ dla modułu).
* Jak uruchomić aplikację - z linii poleceń klasy, mając w nazwie Appliaction z parametrami opisanymi w javadoc w klasie (operowanie na istniejących danych w bazie  ) 

### Z kim się kontaktować? ###

* Właściciel repozytorium kodu - SoftwareSkill
* Autorzy rozwiązania - Krzysztof Kądziołka krzysztof.kadziolka@gmail.com 