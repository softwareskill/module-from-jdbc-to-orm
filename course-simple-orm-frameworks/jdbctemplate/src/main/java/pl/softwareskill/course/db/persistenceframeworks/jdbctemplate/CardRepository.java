package pl.softwareskill.course.db.persistenceframeworks.jdbctemplate;

import java.util.Map;
import java.util.Optional;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.jdbc.core.ArgumentPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
@FieldDefaults(level = PRIVATE, makeFinal = true)
@RequiredArgsConstructor
class CardRepository {

    JdbcTemplate jdbcTemplate;
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    CardMapper mapper = new CardMapper();

    public Optional<Card> findById(String cardId) {
        var card = jdbcTemplate.query("select * from cards where card_id=?",
                new ArgumentPreparedStatementSetter(new Object[]{cardId}), mapper);
        return Optional.ofNullable(card);
    }

    public Optional<Card> findByIdByPropertyBeanMapper(String cardId) {
        var card = jdbcTemplate.queryForObject("select * from cards where card_id=?", new Object[]{cardId},
                BeanPropertyRowMapper.newInstance(Card.class));
        return Optional.ofNullable(card);
    }

    public Optional<Card> findByIdUsingNamedParameters(String cardId) {
        var card = namedParameterJdbcTemplate.queryForObject("select * from cards where card_id=:id",
                Map.of("id", cardId),
                BeanPropertyRowMapper.newInstance(Card.class));
        return Optional.ofNullable(card);
    }

    public void update(Card card) {
        jdbcTemplate.update("update cards set card_uuid=?, card_owner_id=?,enabled=?, country=? where card_id=?",
                ps -> {
                    ps.setString(1, card.getCardUuid());
                    ps.setString(2, card.getCardOwnerId());

                    //Własny typ bool
                    if (card.isEnabled()) ps.setString(3, "Y");
                    else ps.setString(3, "F");
                    //Mapowanie enum
                    ps.setString(4, card.getCountry().name());

                    ps.setString(5, card.getCardId());
                });
    }
}