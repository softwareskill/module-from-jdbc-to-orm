package pl.softwareskill.course.db.persistenceframeworks.jdbctemplate;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
class CardInformationLogger {

    void logCardData(Card card) {
        log.info("Card data for id={} is uuid={}, ownerName={}, active={}, country={}", card.getCardId(), card.getCardUuid(),
                card.getCardOwnerId(), card.isEnabled(), card.getCountry());
    }

    void logCardDataNotFound(String cardId) {
        log.error("Card data for id={} doesn't exist", cardId);
    }
}
