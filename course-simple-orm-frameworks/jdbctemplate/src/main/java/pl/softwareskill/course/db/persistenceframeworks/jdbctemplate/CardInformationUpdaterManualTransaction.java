package pl.softwareskill.course.db.persistenceframeworks.jdbctemplate;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Service
class CardInformationUpdaterManualTransaction {

    CardRepository cardRepository;
    PlatformTransactionManager transactionManager;

    public void updateCardData(String cardId) {
        var transactionTemplate = new TransactionTemplate(transactionManager);
        transactionTemplate.execute(status -> {
            cardRepository.findByIdByPropertyBeanMapper(cardId)
                    .ifPresent(card -> {
                        card.setEnabled(false);
                        card.setCountry(CardCountry.EN);

                        cardRepository.update(card);
                    });
            return true;
        });
    }
}
