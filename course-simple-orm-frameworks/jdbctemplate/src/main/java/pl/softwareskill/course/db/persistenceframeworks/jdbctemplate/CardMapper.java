package pl.softwareskill.course.db.persistenceframeworks.jdbctemplate;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

class CardMapper implements ResultSetExtractor<Card> {

    @Override
    public Card extractData(ResultSet resultSet) throws DataAccessException {
        try {
            if (resultSet.next()) {
                var cardId = resultSet.getString("CARD_ID");
                var cardUUid = resultSet.getString("CARD_UUID");
                var cardOwner = resultSet.getString("CARD_OWNER_ID");
                var cardActive = cardActivityFlag(resultSet);
                var cardCountry = cardCountry(resultSet);
                return new Card(cardId, cardUUid, cardOwner, cardActive, cardCountry);
            } else {
                return null;
            }
        } catch (SQLException exception) {
            throw new DataMappingException(exception);
        }
    }

    private static boolean cardActivityFlag(ResultSet resultSet) throws SQLException {
        var cardEnabled = resultSet.getString("ENABLED");
        return "Y".equalsIgnoreCase(cardEnabled);
    }

    private static CardCountry cardCountry(ResultSet resultSet) throws SQLException {
        var country = resultSet.getString("COUNTRY");
        return CardCountry.valueOf(country);
    }
}
