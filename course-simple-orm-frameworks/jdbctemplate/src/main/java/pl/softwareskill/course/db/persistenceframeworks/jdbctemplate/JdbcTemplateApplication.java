package pl.softwareskill.course.db.persistenceframeworks.jdbctemplate;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa - prezentuje podstawowe możliwości i korzystanie z JdbcTemplate.
 * Obsługa transakcji w ramach Spring
 *
 * Uruchomienie wymaga podania rodzaju operacji i parametrów operacji (kolejne wartości parametrów)
 * 1. r1 odczyt danych karty po id z wykorzystaniem własnego mappera - zobacz CardMapper
 *  - parametry (dodatkowo identyfikator karty)
 * 2. r2 odczyt danych karty po id z wykorzystaniem BeanPropertyRowMapper
 *  - parametry (dodatkowo identyfikator karty)
 * 3. r3 odczyt danych karty po id z wykorzystaniem nazwanych parametrów
 *  - parametry (dodatkowo identyfikator karty)
 * 4. u zmiana danych karty
 *  - parametry (dodatkowo identyfikator karty)
 * 5. mu zmiana danych karty z wykorzystaniem manualnego sterowania transakcjami
 *  - parametry (dodatkowo identyfikator karty)
 */
@SpringBootApplication
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Slf4j
@EnableTransactionManagement //Konfiguracja - obsługa transakcji
public class JdbcTemplateApplication implements CommandLineRunner {

    CardInformationReader reader;
    CardInformationUpdater updater;
    CardInformationUpdaterManualTransaction manualTransactionCardUpdater;

    public static void main(String[] args) {
        SpringApplication.run(JdbcTemplateApplication.class, args);
    }

    @Override
    public void run(String... args) {
        if ("r1".equalsIgnoreCase(args[0])) {
            reader.printCardData(args[1]);
        } else if ("r2".equalsIgnoreCase(args[0])) {
            reader.printCardData2(args[1]);
        } else if ("r3".equalsIgnoreCase(args[0])) {
            reader.printCardData3(args[1]);
        } else if ("u".equalsIgnoreCase(args[0])) {
            updater.updateCard(args[1]);
        } else if ("mu".equalsIgnoreCase(args[0])) {
            manualTransactionCardUpdater.updateCardData(args[1]);
        } else {
            log.error("Nieprawidłowe parametry wywołania");
        }
    }
}
