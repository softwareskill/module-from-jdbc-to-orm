package pl.softwareskill.course.db.persistenceframeworks.jdbctemplate;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Service
@Slf4j
//Kod demonstracyjny
public class CardInformationUpdater {

    CardRepository cardRepository;

    @Transactional
    //Metoda musi być publiczna. Anoracja @Transactional nad tą metodą lub nad klasą (wtedy wszystkie metody publiczne
    //zostaną owinięte transakcją
    public void updateCard(String cardId) {
        cardRepository.findById(cardId)
                .ifPresent(card -> {
                    card.setCountry(CardCountry.PL);
                    card.setCardOwnerId("Krzysztof Kadziolka");

                    cardRepository.update(card);

                    log.info("Zaktualizowano dane karty");
                });
    }
}
