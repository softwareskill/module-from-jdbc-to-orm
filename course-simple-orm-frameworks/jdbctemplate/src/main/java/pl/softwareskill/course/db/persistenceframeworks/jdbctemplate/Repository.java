package pl.softwareskill.course.db.persistenceframeworks.jdbctemplate;

import java.util.Optional;

public interface Repository<T, K> {

    Optional<T> findById(K key);
}
