package pl.softwareskill.course.db.persistenceframeworks.jdbctemplate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
class Card {

    String cardId;
    String cardUuid;
    String cardOwnerId;
    boolean enabled;
    CardCountry country;
}
