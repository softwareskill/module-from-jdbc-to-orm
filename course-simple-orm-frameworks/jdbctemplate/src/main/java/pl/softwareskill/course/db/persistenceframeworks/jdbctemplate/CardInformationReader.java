package pl.softwareskill.course.db.persistenceframeworks.jdbctemplate;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Service
public class CardInformationReader {

    CardInformationLogger logger;
    CardRepository cardRepository;

    public void printCardData(String cardId) {
        cardRepository.findById(cardId)
                .ifPresentOrElse(logger::logCardData,
                        () -> logger.logCardDataNotFound(cardId));
    }

    public void printCardData2(String cardId) {
        cardRepository.findByIdByPropertyBeanMapper(cardId)
                .ifPresentOrElse(logger::logCardData,
                        () -> logger.logCardDataNotFound(cardId));
    }

    public void printCardData3(String cardId) {
        cardRepository.findByIdUsingNamedParameters(cardId)
                .ifPresentOrElse(logger::logCardData,
                        () -> logger.logCardDataNotFound(cardId));
    }
}
