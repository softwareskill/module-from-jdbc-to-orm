package pl.softwareskill.course.db.persistenceframeworks.mybatis;

import java.util.List;
import lombok.Data;
import lombok.ToString;

@Data
@ToString(exclude = {"cards"})
class User {

    Long userId;
    String firstName;
    String lastName;

    List<CreditCard> cards;
}
