package pl.softwareskill.course.db.persistenceframeworks.mybatis;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa - prezentuje podstawowe możliwości i korzystanie z MyBatis z konfiguracją
 * opartą o anotacje, z rozszerzeniami dla Spring (zobacz MyBatisSpringApplicationConfig).
 *
 * Obsługa transakcji w ramach Spring (@Transactional)
 *
 * Uruchomienie wymaga podania rodzaju operacji i parametrów operacji (kolejne wartości parametrów)
 * 1. r odczyt danych karty po id
 *  - parametry (dodatkowo identyfikator karty)
 * 2. u zmiana danych karty
 *  - parametry (dodatkowo identyfikator karty)
 * 3. e dodanie nowego użytkownika
 */
@SpringBootApplication
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Slf4j
@EnableTransactionManagement
public class MyBatisSpringApplication implements CommandLineRunner {

    CardInformationReader cardInformationReader;
    CardInformationUpdater cardInformationUpdater;
    UserExampleService userExampleService;

    public static void main(String[] args) {
        SpringApplication.run(MyBatisSpringApplication.class, args);
    }

    @Override
    public void run(String... args) {
        if ("r".equalsIgnoreCase(args[0])) {
            cardInformationReader.printCardData(args[1]);
        } else if ("u".equalsIgnoreCase(args[0])) {
            cardInformationUpdater.updateCardData(args[1]);
        } else if ("e".equalsIgnoreCase(args[0])) {
            userExampleService.createNewUser();
        }
        else {
            log.error("Wymagany jeden parametr ");
        }
    }
}
