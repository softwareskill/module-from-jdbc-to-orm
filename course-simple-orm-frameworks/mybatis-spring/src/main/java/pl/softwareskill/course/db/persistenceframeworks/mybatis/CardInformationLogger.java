package pl.softwareskill.course.db.persistenceframeworks.mybatis;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
class CardInformationLogger {

    void logCardData(Card card) {
        log.info("Card data for id={} is uuid={}, active={}, country={}", card.getCardId(), card.getCardUuid(),
                card.isEnabled(), card.getCountry());
    }

    void logCardDataNotFound(String cardId) {
        log.error("Card data for id={} doesn't exist", cardId);
    }
}
