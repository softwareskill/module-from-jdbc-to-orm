package pl.softwareskill.course.db.persistenceframeworks.mybatis;

import java.util.Optional;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

@Repository
public interface CardRepository {

    @Select("SELECT * FROM CARDS WHERE CARD_ID = #{cardId}")
    @Results(value = {
            @Result(property = "cardId", column = "CARD_ID"),
            @Result(property = "cardUuid", column = "CARD_UUID"),
            @Result(property = "enabled", column = "ENABLED",
                    javaType = Boolean.class,
                    jdbcType = JdbcType.VARCHAR,
                    typeHandler = YesNoBooleanTypeHandler.class),
            @Result(property = "cardCountry", column = "COUNTRY")
    })
    Optional<Card> findById(String cardId);

    @Update("UPDATE CARDS SET CARD_UUID=#{cardUuid}, " +
            "ENABLED=#{enabled, typeHandler=pl.softwareskill.course.db.persistenceframeworks.mybatis.YesNoBooleanTypeHandler}," +
            "COUNTRY=#{cardCountry} WHERE CARD_ID=#{cardId}")
    void updateCard(Card card);
}
