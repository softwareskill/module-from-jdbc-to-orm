package pl.softwareskill.course.db.persistenceframeworks.mybatis;

import lombok.Data;

@Data
class Card {
    String cardId;
    String cardUuid;
    boolean enabled;
    CardCountry country;
}
