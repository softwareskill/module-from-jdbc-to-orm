package pl.softwareskill.course.db.persistenceframeworks.mybatis;

import lombok.Data;

@Data
class CreditCard {
    String cardId;
    boolean enabled;
    CardCountry cardCountry;
}
