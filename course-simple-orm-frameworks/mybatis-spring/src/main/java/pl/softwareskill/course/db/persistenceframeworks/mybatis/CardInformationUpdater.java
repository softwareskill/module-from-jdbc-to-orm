package pl.softwareskill.course.db.persistenceframeworks.mybatis;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Service
@Slf4j
class CardInformationUpdater {

    CardRepository cardRepository;

    @Transactional
    public void updateCardData(String cardId) {
        cardRepository.findById(cardId)
                .ifPresent(card -> {
                    card.setEnabled(!card.isEnabled());
                    card.setCountry(CardCountry.EN);

                    cardRepository.updateCard(card);
                    log.info("Card data updated");
                });
    }
}
