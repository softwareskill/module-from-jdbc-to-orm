package pl.softwareskill.course.db.persistenceframeworks.mybatis;

import java.time.Instant;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Slf4j
public class UserExampleService {

    UserRepository repository;

    @Transactional
    public void createNewUser() {

        User user = repository.findById(1L).get();

        System.out.println("************************  before ************************ ");

        System.out.println("Dane użytkownika " + user);

        User newUser = new User();
        newUser.setFirstName("KKKK " + Instant.now());
        newUser.setLastName("KKKK " + Instant.now());

        long id = repository.saveUser(newUser);

        System.out.println("Dane nowego użytkownika " + newUser);
    }
}
