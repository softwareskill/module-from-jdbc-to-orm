package pl.softwareskill.course.db.persistenceframeworks.mybatis;

import java.util.List;
import java.util.Optional;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.mapping.FetchType;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository {

    @Select("SELECT * FROM USERS WHERE USER_ID = #{cardId}")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID", id = true),
            @Result(property = "firstName", column = "FIRST_NAME"),
            @Result(property = "lastName", column = "LAST_NAME"),
            @Result(property = "cards", javaType = List.class, column = "USER_ID",
                    many = @Many(select = "getCards", fetchType = FetchType.LAZY))
    })
    Optional<User> findById(Long cardId);

    @Select("SELECT * FROM CREDIT_CARDS")
    @Results(id = "getCards",
            value = {
                    @Result(property = "cardId", column = "CARD_ID", id = true),
                    @Result(property = "enabled", column = "ENABLED",
                            javaType = Boolean.class,
                            jdbcType = JdbcType.VARCHAR,
                            typeHandler = YesNoBooleanTypeHandler.class),
                    @Result(property = "cardCountry", column = "COUNTRY")
            })
    List<CreditCard> getCards();

    @Insert("INSERT INTO USERS(USER_ID, FIRST_NAME, LAST_NAME) values(#{userId}, #{firstName}, #{lastName})")
    @SelectKey(keyProperty = "userId", keyColumn = "USER_ID", before = true, resultType = Long.class, statement = "SELECT nextval('seq_users')")
    Long saveUser(User user);
}
