package pl.softwareskill.course.db.persistenceframeworks.mybatis;

import org.apache.ibatis.session.SqlSession;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import pl.softwareskill.course.db.persistenceframeworks.mybatis.xml.XmlMappingCardDao;

public class CardInformationReaderIntegrationTests {

    private static SqlSession session;

    private final CardInformationLogger logger = Mockito.spy(new CardInformationLogger());
    private CardInformationReader reader;

    @BeforeAll
    static void initDatabase() {
        session = DatabaseInitializer.initializeFromXml();
    }

    @BeforeEach
    void init() {
        CardDao cardRepository = session.getMapper(XmlMappingCardDao.class);
        reader = new CardInformationReader(logger, cardRepository);
    }

    @Test
    void shouldLogCardOwnerData() {
        //given : existing card
        String cardId = existingCard();
        //when: request for card information data
        reader.printCardData(cardId);
        //then: card data is displayed
        verify(logger, times(1)).logCardData(any(Card.class));
    }

    private static String existingCard() {
        return "100";
    }

    @Test
    void shouldLogCardDataNotFound() {
        //given : not existing card
        String cardId = notExistingCard();
        //when: request for card information data
        reader.printCardData(cardId);
        //then: card data not found
        verify(logger, times(1)).logCardDataNotFound(eq(cardId));
    }

    private static String notExistingCard() {
        return "notExisting";
    }
}
