package pl.softwareskill.course.db.persistenceframeworks.mybatis;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CardInformationLogger {

    void logCardData(Card card) {
        log.info("Card data for id={} is uuid={}, ownerName={}, active={}, country={}", card.getCardId(), card.getCardUuid(),
                card.getCardOwner(), card.isEnabled(), card.getCardCountry());
    }

    void logCardDataNotFound(String cardId) {
        log.error("Card data for id={} doesn't exist", cardId);
    }
}
