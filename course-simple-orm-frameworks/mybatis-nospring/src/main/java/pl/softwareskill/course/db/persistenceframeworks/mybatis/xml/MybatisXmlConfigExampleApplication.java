package pl.softwareskill.course.db.persistenceframeworks.mybatis.xml;

import org.apache.ibatis.session.SqlSession;
import pl.softwareskill.course.db.persistenceframeworks.mybatis.CardDao;
import pl.softwareskill.course.db.persistenceframeworks.mybatis.CardInformationLogger;
import pl.softwareskill.course.db.persistenceframeworks.mybatis.CardInformationReader;
import pl.softwareskill.course.db.persistenceframeworks.mybatis.DatabaseInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa - prezentuje podstawowe możliwości i korzystanie z MyBatis z konfiguracją
 * opartą w całości o pliki konfiguracyjne MyBatis (xml)
 *
 * Uruchomienie wymaga podania identyfikatora karty do odczytu
 */
public class MybatisXmlConfigExampleApplication {

    public static void main(String[] args) {

        //Inicjalizacja MyBatis z pliku mybatis-config-byfile.xml
        SqlSession session = DatabaseInitializer.initializeFromXml();

        CardDao cardRepository = session.getMapper(XmlMappingCardDao.class);

        CardInformationLogger logger = new CardInformationLogger();
        CardInformationReader cardInformationReader = new CardInformationReader(logger, cardRepository);

        cardInformationReader.printCardData(args[0]);

        session.close();
    }
}
