package pl.softwareskill.course.db.persistenceframeworks.mybatis;

import java.util.Optional;

public interface CardDao {

    Optional<Card> findById(String cardId);
}
