package pl.softwareskill.course.db.persistenceframeworks.mybatis;

import java.io.IOException;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class DatabaseInitializer {

    //Inicjalizacja na podstawie pliku konfiguracyjnego MyBatis (całośc konfiguracji w pliku)
    public static SqlSession initializeFromXml() {
        try {
            var reader = Resources.getResourceAsReader("mybatis-config-byfile.xml");

            var sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);

            return sqlSessionFactory.openSession();
        } catch (IOException e) {
            throw new DatabaseInitializationException(e);
        }
    }

    //Inicjalizacja na podstawie pliku konfiguracyjnego MyBatis z wykorzystaniem anotacji MyBatis
    public static SqlSession initializeFromAnnotations() {
        try {
            var reader = Resources.getResourceAsReader("mybatis-config-byannotations.xml");

            var sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);

            return sqlSessionFactory.openSession();
        } catch (IOException e) {
            throw new DatabaseInitializationException(e);
        }
    }
}
