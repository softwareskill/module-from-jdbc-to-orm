package pl.softwareskill.course.db.persistenceframeworks.mybatis.annotation;

import org.apache.ibatis.session.SqlSession;
import pl.softwareskill.course.db.persistenceframeworks.mybatis.CardDao;
import pl.softwareskill.course.db.persistenceframeworks.mybatis.CardInformationLogger;
import pl.softwareskill.course.db.persistenceframeworks.mybatis.CardInformationReader;
import pl.softwareskill.course.db.persistenceframeworks.mybatis.DatabaseInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa - prezentuje podstawowe możliwości i korzystanie z MyBatis z konfiguracją
 * opartą o anotacje
 *
 * Uruchomienie wymaga podania identyfikatora karty do odczytu
 */
public class MybatisAnnotationConfig_Example1_Application {

    public static void main(String[] args) {
        //Inicjalizacja MyBatis z pliku mybatis-config-byannotations.xml
        SqlSession session = DatabaseInitializer.initializeFromAnnotations();

        //Wyszukanie mappera (DAO)
        CardDao cardRepository = session.getMapper(AnnotationMappingCardDao.class);

        CardInformationLogger logger = new CardInformationLogger();
        CardInformationReader cardInformationReader = new CardInformationReader(logger, cardRepository);

        //Wyszukanie i wyświetlenie danych
        cardInformationReader.printCardData(args[0]);

        //Zamknięcie sesji i zwolnienie zasobów
        session.close();
    }
}
