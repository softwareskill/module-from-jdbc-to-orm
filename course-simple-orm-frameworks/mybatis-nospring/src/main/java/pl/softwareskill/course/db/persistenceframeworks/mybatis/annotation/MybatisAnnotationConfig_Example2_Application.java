package pl.softwareskill.course.db.persistenceframeworks.mybatis.annotation;

import org.apache.ibatis.session.SqlSession;
import pl.softwareskill.course.db.persistenceframeworks.mybatis.CardCountry;
import pl.softwareskill.course.db.persistenceframeworks.mybatis.DatabaseInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa - prezentuje podstawowe możliwości i korzystanie z MyBatis z konfiguracją
 * opartą o anotacje.
 * Logika wykporzystuje transakcje w MyBatis oraz aktualizację danych karty
 *
 * Uruchomienie wymaga podania identyfikatora karty do odczytu
 */
public class MybatisAnnotationConfig_Example2_Application {

    public static void main(String[] args) {
        //Inicjalizacja MyBatis z pliku mybatis-config-byfile.xml
        SqlSession session = DatabaseInitializer.initializeFromAnnotations();

        //Start transakcji MyBatis
        session.commit(false);

        AnnotationMappingCardDao cardRepository = session.getMapper(AnnotationMappingCardDao.class);

        cardRepository.findById(args[0])
                .ifPresent(card -> {
                    card.setCardCountry(CardCountry.PL);
                    card.setEnabled(!card.isEnabled());

                    cardRepository.updateCard(card);
                });

        //commit
        session.commit();

        //Zamknięcie sesji i zwolnienie zasobów
        session.close();
    }
}
