package pl.softwareskill.course.db.persistenceframeworks.mybatis;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Card {
    String cardId;
    String cardUuid;
    String cardOwner;
    boolean enabled;
    CardCountry cardCountry;
}
