CREATE TABLE CARDS (
    CARD_ID VARCHAR(20) NOT NULL PRIMARY KEY,
    CARD_UUID VARCHAR(40) NOT NULL,
    ENABLED VARCHAR(1) NOT NULL,
    COUNTRY VARCHAR(2) NOT NULL,
    CARD_BALANCE DECIMAL(10,2) NOT NULL,
    CARD_OWNER_ID VARCHAR(20)
);

INSERT INTO cards (card_id, card_uuid, enabled, country, card_balance, CARD_OWNER_ID) VALUES ('2'	,'8861e09d-6d6e-47c3-ba06-18dc6374a888', 'Y', 'PL',	900.00, 'Softwareskill');
INSERT INTO cards (card_id, card_uuid, enabled, country, card_balance, CARD_OWNER_ID) VALUES ('1'	,'8861e09d-6d6e-47c3-ba06-18dc6374a939', 'Y', 'EN',	742.20, 'Softwareskill');

CREATE TABLE users (
    user_id DECIMAL(22,0) NOT NULL PRIMARY KEY,
    first_name VARCHAR(100) NOT NULL,
    last_name VARCHAR(100) NOT NULL
);

INSERT INTO users (user_id, first_name, last_name) VALUES (1, 'Krzysztof', 'Kadziolka');

CREATE TABLE credit_cards (
    card_id VARCHAR(20) NOT NULL PRIMARY KEY,
    enabled VARCHAR(1) NOT NULL,
    country VARCHAR(2) NOT NULL,
    user_id DECIMAL(22,0) NOT NULL
);

ALTER TABLE credit_cards ADD CONSTRAINT credit_cards_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(user_id);

INSERT INTO credit_cards (card_id, enabled, country, user_id) VALUES ('1',	'Y',	'PL',	1);

