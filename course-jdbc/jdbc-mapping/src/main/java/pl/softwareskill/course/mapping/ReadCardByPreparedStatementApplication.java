package pl.softwareskill.course.mapping;

import java.sql.ResultSet;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje wyszukiwanie karty z wykorzystaniem PreparedStatement
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class ReadCardByPreparedStatementApplication {

    public static void main(String[] args) throws Exception {
        //Nawiązanie połączenia (PostgreSQL + Hikari, parametry w pliku datasource.properties)
        var connection = HikariDatasourceConnection.getConnection();
        //PreparedStatement - jeden parametr ?, nie ma konieczności używania apostrofów
        final String query = "select * from cards where card_id=?";

        try (var statement = connection.prepareStatement(query)) {

            //Wstrzyknięcie wartości parametru - sterownik JDBC wstrzyknie odpowiednią wartość i doda apostrofy
            statement.setString(1, "1");

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    var id = resultSet.getString("CARD_ID");
                    var uuid = resultSet.getString("CARD_UUID");
                    var enabed = resultSet.getString("ENABLED");
                    var country = resultSet.getString("COUNTRY");
                    var balance = resultSet.getBigDecimal("CARD_BALANCE");

                    var text = String.format("Dane karty id=%s, uuid=%s, enabled=%s, country=%s, balance=%f",
                            id, uuid, enabed, country, balance);
                    System.out.println(text);
                } else {
                    System.err.println("Nie znaleziono karty");
                }
            }
        }

        //Zwolnienie połączenia
        connection.close();

        //Zwolnienie puli
        HikariDatasourceConnection.close();
    }
}