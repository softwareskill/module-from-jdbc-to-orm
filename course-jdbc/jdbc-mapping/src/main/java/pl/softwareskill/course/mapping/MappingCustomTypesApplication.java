package pl.softwareskill.course.mapping;

import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje własne mapowanie dla danych pobieranych z bazy danych dla enum oraz własnego
 * typu logicznego.
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class MappingCustomTypesApplication {

    public static void main(String[] args) throws Exception {
        //Nawiązanie połączenia
        var connection = HikariDatasourceConnection.getConnection();

        try (Statement statement = connection.createStatement()) {

            //wyszukanie karty
            statement.execute(String.format("select * from cards where card_id='%s'", "1"));

            try (ResultSet resultSet = statement.getResultSet()) {
                if (resultSet.next()) {
                    var id = resultSet.getString("CARD_ID");
                    var uuid = resultSet.getString("CARD_UUID");
                    var balance = resultSet.getBigDecimal("CARD_BALANCE");

                    //Własny typ logiczny Y/N
                    var enabed = resultSet.getBoolean("ENABLED");

                    System.out.println("Wartość dla " + resultSet.getString("ENABLED") + " to " + enabed);

                    //Enum z krajami
                    var country = Country.valueOf(resultSet.getString("COUNTRY"));
                }
            }
        }

        connection.close();

        HikariDatasourceConnection.close();
    }
}