package pl.softwareskill.course.mapping;

import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje aktualizację danych klienta (dużo kolumn) karty z wykorzystaniem PreparedStatement
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class UpdateCustomerByPreparedStatementApplication {

    public static void main(String[] args) throws Exception {
        //Nawiązanie połączenia (PostgreSQL + Hikari, parametry w pliku datasource.properties)
        var connection = HikariDatasourceConnection.getConnection();

        //Zapytanie - w miejsce parametrów znaki zapytania, nie ,ma konieczności dodawania apostrofów
        final String updateSql = "UPDATE CUSTOMERS SET " +
                "    FIRST_NAME =?, " +
                "    LAST_NAME =?, " +
                "    EMAIL_ADDRESS =?, " +
                "    CARD_ID =?, " +
                "    STREET =?, " +
                "    STREET_NUMBER =?, " +
                "    CITY =?, " +
                "    COUNTRY =?, " +
                "    POSTAL_CODE =?, " +
                "    UPDATED_AT =? " + //Data będzie obsłużona przez sterownik JDBC
                " WHERE CUSTOMER_ID=?";
        try (PreparedStatement statement = connection.prepareStatement(updateSql)) {
            statement.setString(1, "Krzysztof");
            statement.setString(2, "Kadziolka");
            statement.setString(3, "krzysztof.kadziolka@gmail.com");
            statement.setString(4, "1");
            statement.setString(5, "Adresowa");
            statement.setString(6, "1A");
            statement.setString(7, "Katowice");
            statement.setString(8, "PL");
            statement.setString(9, "00-089");
            //Data  obsłużona przez sterownik JDBC
            statement.setTimestamp(10, Timestamp.valueOf(LocalDateTime.now()));
            statement.setLong(11, 1L);

            int updateCount = statement.executeUpdate();

            if (updateCount == 0) {
                System.err.println("Nie znaleziono danych do aktualizacji");
            } else {
                System.out.println("Zmodyfikowano pomyślnie dane");
            }
        }

        //Zwolnienie połączenia
        connection.close();

        //Zwolnienie puli
        HikariDatasourceConnection.close();
    }
}