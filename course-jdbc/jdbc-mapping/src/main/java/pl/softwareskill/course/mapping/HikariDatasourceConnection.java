package pl.softwareskill.course.mapping;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import static java.util.Objects.isNull;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HikariDatasourceConnection {

    private static final Logger LOGGER = LoggerFactory.getLogger(HikariDatasourceConnection.class);
    private static HikariDataSource dataSource;

    public static Connection getConnection() throws SQLException, IOException {
        if (isNull(dataSource)) {
            initializeDatasource();
        }
        var connection = dataSource.getConnection();
        LOGGER.info("Connected to database");
        return connection;
    }

    private static void initializeDatasource() throws IOException {
        var connectionProps = loadConfigurationProperties();
        var hikariConfig = new HikariConfig(connectionProps);
        dataSource = new HikariDataSource(hikariConfig);
        LOGGER.info("Datasource initialized");
    }

    private static Properties loadConfigurationProperties() throws IOException {
        try (InputStream configurationInputStream = HikariDatasourceConnection.class.getClassLoader().getResourceAsStream("datasource.properties")) {
            Properties connectionProps = new Properties();
            connectionProps.load(configurationInputStream);
            LOGGER.debug("Database configuration read successfully");
            return connectionProps;
        }
    }

    public static void close() {
        dataSource.close();
    }
}
