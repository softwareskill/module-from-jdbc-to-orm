package pl.softwareskill.course.crud;

import java.sql.Connection;
import java.sql.SQLException;
import static java.util.Objects.isNull;

/**
 * Kod demonstracyjny
 *
 * Aplikacja ustawia status zamówienia na zapłacone
 *
 * 1. Sprawdzenie istnienia oraz statusu zamówienia
 * 2. Jeżeli status WAITING_FOR_PAYMENT to zmiana stanu zamówienia mna PAID
 *
 * W przypadku błędu wyjście z programu z komunikatem błędu.
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class SetOrderStatusPaidApplication {

    public static void main(String[] args) throws Exception {
        //Nawiązanie połączenia
        var connection = CrudDatasource.connectToDatabase();

        //rozpoczęcie transakcji JDBC
        connection.setAutoCommit(false);

        //Pobranie statusu zamówienia
        var orderStatus = getOrderStatus(connection);

        //Weryfikacja statusu zamówienia
        if (!validateStatus(orderStatus)) {
            connection.rollback();
            System.exit(-1);
        }

        try (var statement = connection.createStatement()) {
            //CRUD DELETE/DESTROY - zapytanie SQL
            String sqlQuery = "UPDATE ORDERS SET STATUS='PAID' WHERE ORDER_NUMBER='2'";

            //Wykonanie zapytania
            statement.execute(sqlQuery);

            //Commit JDBC
            connection.commit();

            System.out.println("Status zamówienia zaktualizowany");
        }
    }

    private static String getOrderStatus(Connection connection) throws SQLException {
        try (var statement = connection.createStatement()) {
            //CRUD READ/RETRIEVE - zapytanie SQL
            String sqlQuery = "SELECT STATUS FROM ORDERS WHERE ORDER_NUMBER='2'";

            //Wykonanie zapytania
            statement.execute(sqlQuery);

            //Weryfikacja wyniku
            try (var resultSet = statement.getResultSet()) {

                //Pobieramy wartość
                if (resultSet.next()) { //Zamówienie znalezione
                    //Zwracamy status
                    return resultSet.getString(1);
                } else {
                    return null;
                }
            }
        }
    }

    private static boolean validateStatus(String orderStatus) {
        if (isNull(orderStatus)) {
            System.err.println("Zamówienie nie istnieje");
            return false;
        } else if (!"WAITING_FOR_PAYMENT".equals(orderStatus)) {
            System.err.println("Nieprawidłowy status");
            return false;
        }
        return true;
    }
}
