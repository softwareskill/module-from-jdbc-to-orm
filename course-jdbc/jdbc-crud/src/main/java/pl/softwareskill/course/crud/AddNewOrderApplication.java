package pl.softwareskill.course.crud;

import java.sql.Connection;

/**
 * Kod demonstracyjny
 *
 * Aplikacja dodaje nowe zamówienie o id = XYZ i ile nie istnieje już takie w bazie.
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class AddNewOrderApplication {

    public static void main(String[] args) throws Exception {
        //Nawiązanie połączenia
        var connection = CrudDatasource.connectToDatabase();

        //rozpoczęcie transakcji JDBC
        connection.setAutoCommit(false);

        if (isOrderExists(connection)) {
            System.err.println("Zamówienie o id = 'XYZ' istnieje");

            connection.rollback();

            System.exit(-1);
        }

        try (var statement = connection.createStatement()) {
            //CRUD CREATE - zapytanie SQL
            String sqlQuery = "INSERT INTO ORDERS(ORDER_NUMBER,AMOUNT_TO_PAY,STATUS) " +
                    "VALUES ('XYZ', 15.67, 'WAITING_FOR_PAYMENT')";

            //Wykonanie zapytania
            statement.execute(sqlQuery);

            //commit transakcji JDBC
            connection.commit();

            System.out.println("Zamówienie dodane");
        }

        //Zwolnienie zasobów
        connection.close();
    }

    private static boolean isOrderExists(Connection connection) throws Exception {
        try (var statement = connection.createStatement()) {
            //CRUD READ/RETRIEVE - zapytanie SQL
            String sqlQuery = "SELECT 1 FROM ORDERS WHERE ORDER_NUMBER='XYZ'";

            //Wykonanie zapytania
            statement.execute(sqlQuery);

            //Weryfikacja wyniku
            try (var resultSet = statement.getResultSet()) {

                //Nieistotna wartość z ResultSet - wystarczy że niepusty wynik
                return resultSet.next();
            }
        }
    }
}
