package pl.softwareskill.course.jdbc.transaction;

import java.io.IOException;
import java.sql.SQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje obsługę transkacji w JDBC - obsługa zapłaty za zamówienie.
 *
 * Kroki logiki
 * 1. Rozpoczęcie transakcji i wyszukanie użytkownika
 * 2. Pobranie wartości zamówienia
 * 3. Odjęcie kwoty zamówienia od karty
 * 4. Sprawdzenie czy saldo karty nie jest ujemne
 * 5. Jeżeli błąd 1-4 wyjście z rollback i komunikatem
 * 6. Jeżeli nie było błędu wysłanie maila, commit i wyjście z programu
 *
 * PaymentOrder - główna klasa obsługująca logikę
 * MailNotifier - symulacja wysłania maila (log na konsolę)
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class BookstoreTransactionalApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookstoreTransactionalApplication.class);

    public static void main(String[] args) throws IOException, SQLException {
        //Nawiązanie połączenia (PostgreSQL + Hikari, parametry w pliku datasource.properties)
        var connection = HikariDatasourceConnection.getConnection();

        LOGGER.info("Połączono do bazy danych z wykorzystaniem puli połączeń");

        var cardId = "1";
        var orderNumber = "1";

        var notifier = new MailNotifier();
        var paymentOrder = new PaymentOrder(connection, notifier);

        paymentOrder.payForOrder(cardId, orderNumber);

        //Zwolnienie połączenia
        connection.close();

        //Zwolnienie puli
        HikariDatasourceConnection.close();
    }
}
