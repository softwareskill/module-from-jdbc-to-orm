package pl.softwareskill.course.jdbc.transaction;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Locale;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//Kod demonstracyjny
class PaymentOrder {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookstoreTransactionalApplication.class);
    private final Connection connection;
    private final MailNotifier notifier;

    PaymentOrder(Connection connection, MailNotifier notifier) {
        this.connection = connection;
        this.notifier = notifier;
    }

    void payForOrder(String cardId, String orderNumber) {
        try {
            var emailAddress = getUserEmail(cardId);

            //Pobranie wartości zamówienia
            var orderValue = getOrderValue(orderNumber);

            //START TRANSAKCJI
            connection.setAutoCommit(false);

            try (var statement = connection.createStatement()) {

                //Obciążenie karty - saldo = saldo - wartość zamówienia
                statement.execute(String.format(Locale.US, "UPDATE CARDS SET CARD_BALANCE=CARD_BALANCE-%.2f WHERE CARD_ID='%s'", orderValue, cardId));

                //Pobranie salda
                var cardBalance = getCardBalance(cardId);

                //Weryfikacja czy nie powstał debet
                if (cardBalance.compareTo(0f) >= 0) {

                    //Wysłanie powiadomienia
                    notifier.sendOrderPaidMessage(emailAddress, cardId);//Ta logika powinna być delegowana do osobnej transakcji

                    //Zmiana statusu zamówienia
                    statement.execute(String.format("UPDATE ORDERS SET ORDER_STATUS='PAID' WHERE ORDER_NUMBER='%s'", cardId));

                    //COMMIT
                    connection.commit();

                    LOGGER.info("Zakończono pomyślnie proces zapłaty za zamówienie");
                } else {
                    //ROLLBACK
                    connection.rollback();

                    LOGGER.error("Niewystarczające środki na karcie");
                }
            }
        } catch (SQLException sqlException) {
            try {
                //ROLLBACK
                connection.rollback();
                LOGGER.error("Bład zapłaty za zamówienie", sqlException);
            } catch (SQLException e) {
                LOGGER.error("Bład wycofacia transakcji", sqlException);
            }
        }
    }

    private String getUserEmail(String cardId) throws SQLException {
        try (var statement = connection.createStatement()) {

            statement.execute(String.format("SELECT EMAIL_ADDRESS FROM CUSTOMERS WHERE CARD_ID='%s'", cardId));

            try (var resultSet = statement.getResultSet()) {
                if (resultSet.next()) {
                    return resultSet.getString(1);
                }
            }
        }
        throw new IllegalStateException("Błąd pobierania adresu email");
    }

    private Float getOrderValue(String orderNumber) throws SQLException {
        try (var statement = connection.createStatement()) {

            statement.execute(String.format("SELECT AMOUNT_TO_PAY FROM ORDERS WHERE ORDER_NUMBER='%s'", orderNumber));

            try (var resultSet = statement.getResultSet()) {
                if (resultSet.next()) {
                    return resultSet.getFloat(1);
                }
            }
        }
        throw new IllegalStateException("Błąd pobierania wartości zamówienia");
    }

    private Float getCardBalance(String cardId) throws SQLException {
        try (var statement = connection.createStatement()) {

            statement.execute(String.format("SELECT CARD_BALANCE FROM CARDS WHERE CARD_ID='%s'", cardId));

            try (var resultSet = statement.getResultSet()) {
                if (resultSet.next()) {
                    return resultSet.getFloat(1);
                }
            }
        }
        throw new IllegalStateException("Błąd pobierania wartości zamówienia");
    }
}
