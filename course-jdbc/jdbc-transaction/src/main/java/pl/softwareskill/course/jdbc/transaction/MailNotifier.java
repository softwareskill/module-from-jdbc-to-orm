package pl.softwareskill.course.jdbc.transaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MailNotifier {

    private static final Logger LOGGER = LoggerFactory.getLogger(MailNotifier.class);

    void sendOrderPaidMessage(String emailAddress, String orderNumber) {
        LOGGER.info("Powiadomienie o zapłacie zamówienia {} na adres email={} wysłane", orderNumber, emailAddress);
    }
}
