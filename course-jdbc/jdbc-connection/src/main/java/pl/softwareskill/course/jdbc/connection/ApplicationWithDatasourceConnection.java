package pl.softwareskill.course.jdbc.connection;

import com.zaxxer.hikari.HikariDataSource;
import java.sql.SQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Aplikacja demonstracyjna
 *
 * Funkcjonalność - połączenie do bazy danych H2 w trybie pamięciowym z wykorzystaniem DataSource Hikari
 *
 * Aplikacja nie wymaga podania parametrów
 */
public class ApplicationWithDatasourceConnection {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationWithDatasourceConnection.class);

    public static void main(String[] args) throws SQLException {
        //Kod demonstracyjny

        //Implementacja DataSource
        var dataSource = new HikariDataSource();

        //Ustawienie parametrów połączenia do bazy danych dla datasource

        //Jeżeli biblioteka JDBC zawiera plik META-INF/services/java.sqlDriver to rejestracja nie jest konieczna.
        //Chyba że biblioteka zawiera kilka klas i chcemy wybrać właściwą
        dataSource.setDriverClassName("org.h2.Driver");

        dataSource.setJdbcUrl("jdbc:h2:mem:softwareskill_orm");
        dataSource.setUsername("ormowner");
        dataSource.setPassword("");

        //Maksymalny rozmiar puli połączeń
        dataSource.setMaximumPoolSize(6);

        //Nawiązanie połączenia z bazą danych
        var connection = dataSource.getConnection();
        LOGGER.info("Połączono do bazy danych z wykorzystaniem puli połączeń");
    }
}
