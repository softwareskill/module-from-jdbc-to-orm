package pl.softwareskill.course.jdbc.connection;

import java.sql.DriverManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Aplikacja demonstracyjna
 *
 * Funkcjonalność - połączenie do bazy danych H2 w trybie pamięciowym
 *
 * Aplikacja nie wymaga podania parametrów
 */
public class ApplicationWithBasicJdbcConnection {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationWithBasicJdbcConnection.class);

    public static void main(String[] args) throws Exception {
        //Kod demonstracyjny

        //rejestracja klasy sterownika
        Class.forName("org.h2.Driver");

        //nawiązanie połączenia
        var connection = DriverManager.getConnection(
                "jdbc:h2:mem:softwareskill_orm",
                "ormowner",
                "");

        LOGGER.info("Połączono do bazy danych z wykorzystaniem połączenia JDBC");
    }
}
