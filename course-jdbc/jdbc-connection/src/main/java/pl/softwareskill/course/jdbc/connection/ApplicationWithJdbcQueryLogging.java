package pl.softwareskill.course.jdbc.connection;

import com.p6spy.engine.common.StatementInformation;
import com.p6spy.engine.event.JdbcEventListener;
import com.p6spy.engine.logging.LoggingEventListener;
import com.p6spy.engine.spy.JdbcEventListenerFactory;
import com.p6spy.engine.spy.P6SpyDriver;
import java.sql.DriverManager;
import static java.util.Objects.isNull;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Aplikacja demonstracyjna
 *
 * Funkcjonalność - połączenie do bazy danych PostgreSQL z wykorzystaniem sterownika proxy P6Spy.
 *
 * Logika prezentuje zliczanie liczby zapytań SELECT wysyłanych do bazy danych z wykorzystaniem sterownika proxy.
 *
 * Aplikacja nie wymaga podania parametrów
 */
public class ApplicationWithJdbcQueryLogging {

    public static void main(String[] args) throws Exception {
        //Kod demonstracyjny
        //nawiązanie połączenia do bazy PostgreSQL

        AtomicInteger selectCount = new AtomicInteger();//Licznik zapytań

        //Dodatkowa funkcjonalność sterownika P6Spy - event listener
        P6SpyDriver.setJdbcEventListenerFactory(new JdbcEventListenerFactory() {
            @Override
            public JdbcEventListener createJdbcEventListener() {
                return new LoggingEventListener() {
                    @Override
                    public void onBeforeAnyExecute(StatementInformation statementInformation) {
                        super.onBeforeAnyExecute(statementInformation);
                        if (isSelect(statementInformation)) {
                            selectCount.incrementAndGet();//inkrementacja licznika
                        }
                    }

                    private boolean isSelect(StatementInformation statementInformation) {
                        var sql = statementInformation.getSql();
                        if (isNull(sql)) {
                            return false;
                        }
                        return sql.toLowerCase().startsWith("select ");//weryfikacja czy zapytanie SELECT
                    }
                };
            }
        });
        var connection = DriverManager.getConnection(
                "jdbc:p6spy:postgresql:softwareskill",
                "softwareskill",
                "softwareskill");
        var before = selectCount.get();
        connection.createStatement().executeQuery("Select 1 ");
        var after = selectCount.get();
        System.out.println("Różnica " + (after - before));
    }
}
