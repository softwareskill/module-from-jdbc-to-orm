package pl.softwareskill.course.jdbc.datasource.h2;

import java.sql.SQLException;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentująca połączenie do bazy danych z wykorzystaniem natywnego źródła danych dla bazy danych H2
 * (dostępne w ramach sterownika JDBC dla H2)
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
import org.h2.jdbcx.JdbcDataSource;

public class H2DataSourceApplication {

    public static void main(String[] args) throws SQLException {
        //Kod demonstracyjny

        //Implementacja natywnego DataSource dla bazy H2
        var dataSource = new JdbcDataSource();

        dataSource.setURL("jdbc:h2:mem:softwareskill_orm;TRACE_LEVEL_SYSTEM_OUT=3");
        dataSource.setLoginTimeout(1000);

        var connection = dataSource.getConnection();
    }
}
