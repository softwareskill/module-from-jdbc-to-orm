package pl.softwareskill.course.jdbc.datasource.pg;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import javax.sql.PooledConnection;

public class PostgresSessionChecker {

    static void checkSessionData(PooledConnection pooledConnection) throws SQLException {
        Connection connection = pooledConnection.getConnection();
        checkSessionData(connection);
    }

    static void checkSessionData(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        var rs = statement.executeQuery("SELECT * FROM ORDERS WHERE ORDER_NUMBER='NEW_POOLED0'");
        boolean orderExists = rs.next();
        rs.close();
        statement.close();
        System.out.println("Zamówienie istnieje " + orderExists);
    }

    static void initSessionCheckData(int customIdValue, PooledConnection pooledConnection) throws SQLException {
        Connection connection = pooledConnection.getConnection();
        initSessionCheckData(customIdValue, connection);
    }

    static void initSessionCheckData(int customIdValue, Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        statement.execute("INSERT INTO ORDERS(ORDER_NUMBER,AMOUNT_TO_PAY,ORDER_STATUS) " +
                "VALUES('NEW_POOLED" + customIdValue + "',99.99,'WAITING_FOR_PAYMENT');");
        statement.close();
    }
}
