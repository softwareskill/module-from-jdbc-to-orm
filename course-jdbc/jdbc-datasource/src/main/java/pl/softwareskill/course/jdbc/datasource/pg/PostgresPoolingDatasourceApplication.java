package pl.softwareskill.course.jdbc.datasource.pg;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.PooledConnection;
import org.postgresql.ds.PGConnectionPoolDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentująca połączenie do bazy danych z wykorzystaniem natywneg puli połączeń bazy danych PostgreSQL
 * (dostępne w ramach sterownika JDBC dla Postgres).
 *
 * Prezentuje taże możliwości konfiguracji tej natywnej puli z wykorzystaniem PGConnectionPoolDataSource
 * oraz zachowanie dla ustawiania zmiennych w sesji bazodanowej.
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class PostgresPoolingDatasourceApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(PostgresPoolingDatasourceApplication.class);

    public static void main(String[] args) throws SQLException, InterruptedException {
        //Kod demonstracyjny

        //Implementacja DataSource dla natywnej puli połączeń dla PostgreSQL
        var dataSource = new PGConnectionPoolDataSource();

        //Ustawienie parametrów połączenia do bazy danych dla datasource
        dataSource.setURL("jdbc:postgresql:softwareskill?loggerLevel=trace");
        dataSource.setUser("softwareskill");
        dataSource.setPassword("softwareskill");
        dataSource.setDefaultAutoCommit(false);

        List<PooledConnection> connections = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            //Nawiązanie połączenia z bazą danych
            var connection = dataSource.getPooledConnection();
            connection.getConnection().setAutoCommit(false);
            //Ustawienie zmiennej sesyjnej Postgres
            PostgresSessionChecker.initSessionCheckData(i, connection);
            connections.add(connection);
        }

        Thread.sleep(1000);

        connections.get(0).getConnection().close();

        var connection = dataSource.getPooledConnection();
        try {
            //Weryfikacja wartości zmiennej sesyjnej Postgres
            PostgresSessionChecker.checkSessionData(connection);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        LOGGER.info("Połączono do bazy danych z wykorzystaniem puli połączeń");
    }
}
