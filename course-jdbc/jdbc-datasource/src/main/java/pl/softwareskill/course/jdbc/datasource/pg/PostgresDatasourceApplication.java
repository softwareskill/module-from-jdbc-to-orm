package pl.softwareskill.course.jdbc.datasource.pg;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.postgresql.ds.PGSimpleDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentująca połączenie do bazy danych z wykorzystaniem puli połączeń Hikari i bazy danych PostgreSQL,
 * a także elementy jakie, można dla Hikari skonfigurować.
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class PostgresDatasourceApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(PostgresDatasourceApplication.class);

    public static void main(String[] args) throws SQLException, InterruptedException {
        //Kod demonstracyjny

        //Implementacja DataSource
        var dataSource = new PGSimpleDataSource();

        //Ustawienie parametrów połączenia do bazy danych dla datasource
        dataSource.setURL("jdbc:postgresql:softwareskill?loggerLevel=trace");
        dataSource.setUser("softwareskill");
        dataSource.setPassword("softwareskill");
        dataSource.setConnectTimeout(1000);
        dataSource.setSocketTimeout(1000);
        dataSource.setLoginTimeout(1000);
        dataSource.setPreparedStatementCacheQueries(100);
        dataSource.setTcpKeepAlive(true);

        List<Connection> connections = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            //Nawiązanie połączenia z bazą danych
            Connection connection = dataSource.getConnection();
            connections.add(connection);
        }

        Thread.sleep(1000);

        connections.get(0).close();

        var connection = dataSource.getConnection();
        LOGGER.info("Połączono do bazy danych z wykorzystaniem puli połączeń");
    }
}
