package pl.softwareskill.course.jdbc.statements;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje podstawowy tryb wywołania dla wyrażenia - execute + weryfikacja liczby zaktualizowanych wierszy getUpdateCount
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class StatementNoResult_Example2_Application {

    public static void main(String[] args) throws Exception {
        //Nawiązanie połączenia (PostgreSQL + Hikari, parametry w pliku datasource.properties)
        var connection = HikariDatasourceConnection.getConnection();

        try (var statement = connection.createStatement()) {
            //Zapytanie SQL
            String sqlQuery = "UPDATE ORDERS SET AMOUNT_TO_PAY=12.67 WHERE ORDER_NUMBER='1111'";

            //Wykonanie zapytania -  wiadomo że UPDATE więc będzie 0
            var status = statement.execute(sqlQuery);

            //Pobranie liczby zaktuyalizowanych wierszy
            var count = statement.getUpdateCount();

            //Wiemy że instrukcja to UPDATE true oznacza że zmodyfikowano wiersze, false brak modyfikacji
            if (count > 0) {
                System.out.println("Zaktualizowano kwotę do zapłaty");
            } else {
                System.err.println("Nie znaleziono rekordu do aktualizacji kwoty do zapłaty ");
            }
        }

        //Zwolnienie połączenia
        connection.close();

        //Zwolnienie puli
        HikariDatasourceConnection.close();
    }
}
