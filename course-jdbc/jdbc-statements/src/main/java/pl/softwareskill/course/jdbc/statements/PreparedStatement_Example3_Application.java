package pl.softwareskill.course.jdbc.statements;

import java.sql.JDBCType;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje pobieranie metadanych dla wyrażenia prekompilowanego (PreparedStatement)
 * - aktualizacja kwoty zamówienia dla bazy danych PostgreSQL (tabela ORDERS)
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class PreparedStatement_Example3_Application {

    public static void main(String[] args) throws Exception {
        //Nawiązanie połączenia (PostgreSQL + Hikari, parametry w pliku datasource.properties)
        var connection = HikariDatasourceConnection.getConnection();

        //Zapytanie SQL - znak zapytania dla wstrzyknięcia parametrów, apostrofy dodane zostaną przez sterownik
        String sqlQuery = "UPDATE ORDERS SET AMOUNT_TO_PAY=? WHERE ORDER_NUMBER=?";

        try (var statement = connection.prepareStatement(sqlQuery)) {

            //Sprawdzenie metadanych parametrów
            var metadata = statement.getParameterMetaData();

            System.out.println("Liczba parametrów " + metadata.getParameterCount());
            for (int i = 1; i <= metadata.getParameterCount(); i++) {
                System.out.println("Typ parametru " + i + " to " + JDBCType.valueOf(metadata.getParameterType(i)));
            }
        }

        //Zwolnienie połączenia
        connection.close();

        //Zwolnienie puli
        HikariDatasourceConnection.close();
    }
}
