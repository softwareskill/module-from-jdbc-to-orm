package pl.softwareskill.course.jdbc.statements;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje ograniczenie rozmiaru kolumny przez JDBC dla zapytania (maxFieldSize)
 * dla sterownika JDBC bazy PostgreSQL
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class StatementLargeColumnExampleApplication {

    public static void main(String[] args) throws Exception {
        //Nawiązanie połączenia (PostgreSQL + Hikari, parametry w pliku datasource.properties)
        var connection = HikariDatasourceConnection.getConnection();

        try (var statement = connection.createStatement()) {
            //Zapytanie SQL
            String sqlQuery = "SELECT ID, CONTENT FROM BIGTABLE";

            statement.setMaxFieldSize(22);//Ustawienie maksymalnego rozmiaru

            //Wykonanie zapytania
            var status = statement.execute(sqlQuery);

            int rowCount = 0;

            //Pobranie wyniku
            try (var result = statement.getResultSet()) {

                if (status) {
                    while (result.next()) {

                        var contentValue = result.getString(2);

                        System.out.println("Content value - string " + contentValue);
                    }
                }
            }
        }

        //Zwolnienie połączenia
        connection.close();

        //Zwolnienie puli
        HikariDatasourceConnection.close();
    }
}
