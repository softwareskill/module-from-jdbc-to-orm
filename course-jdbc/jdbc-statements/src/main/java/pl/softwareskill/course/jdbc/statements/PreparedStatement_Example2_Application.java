package pl.softwareskill.course.jdbc.statements;

import java.math.BigDecimal;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje aktualiozację kwoty zamówienia dla bazy danych PostgreSQL
 * (tabela ORDERS) z wykorzystaniem wyrażenia prekompilowanego (PreparedStatement)
 * i trybu wywołania z aktualizacją executeUpdate
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class PreparedStatement_Example2_Application {

    public static void main(String[] args) throws Exception {
        //Nawiązanie połączenia (PostgreSQL + Hikari, parametry w pliku datasource.properties)
        var connection = HikariDatasourceConnection.getConnection();

        //Zapytanie SQL - znak zapytania dla wstrzyknięcia parametrów, apostrofy dodane zostaną przez sterownik
        String sqlQuery = "UPDATE ORDERS SET AMOUNT_TO_PAY=? WHERE ORDER_NUMBER=?";

        try (var statement = connection.prepareStatement(sqlQuery)) {

            //Ustawienie parametrów
            statement.setBigDecimal(1, new BigDecimal(12.34d));
            statement.setString(2, "1");

            //Wykonanie zapytania -  wiadomo że UPDATE więc będzie > 0 o ile będa jakieś zaktualizowane wiersze
            var count = statement.executeUpdate();

            //Wiemy że instrukcja to UPDATE true oznacza że zmodyfikowano wiersze, false brak modyfikacji
            if (count > 0) {
                System.out.println("Zaktualizowano kwotę do zapłaty");
            } else {
                System.err.println("Nie znaleziono rekordu do aktualizacji kwoty do zapłaty ");
            }
        }

        //Zwolnienie połączenia
        connection.close();

        //Zwolnienie puli
        HikariDatasourceConnection.close();
    }
}
