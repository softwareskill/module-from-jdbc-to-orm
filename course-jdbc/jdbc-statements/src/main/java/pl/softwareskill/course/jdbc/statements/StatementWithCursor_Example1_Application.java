package pl.softwareskill.course.jdbc.statements;

import java.math.BigDecimal;
import java.sql.ResultSet;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje tryb wywołania wyrażenia z kursorem oraz aktualizacją danych z wiersza dla sterownika PostgreSQL
 *
 * Rozszerzone logowanie pozawala na weryfikację jakie zapytania zostaną wysłane do bazy danych.
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class StatementWithCursor_Example1_Application {

    public static void main(String[] args) throws Exception {
        //Nawiązanie połączenia (PostgreSQL + Hikari + Sterownik P6Spy dla poszerzonego logowanias, parametry w pliku datasource_spy.properties)
        var connection = HikariDatasourceConnection.getTracingConnection();

        //Bez ustawiania ResultSet.CONCUR_UPDATABLE wywali się
        try (var statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE)) {
            //Zapytanie SQL

            String sqlQuery =
                    "SELECT " +
                            //PostgreSQL wymaga aby w wyniku był klucz główny a FOR UPDATE jest na końcu
                            "ORDER_NUMBER, " +

                            "AMOUNT_TO_PAY " +
                            "FROM ORDERS WHERE ORDER_NUMBER='1' " +
                            //PostgreSQL wymaga aby FOR UPDATE był na końcu
                            "FOR UPDATE";

            //Nazwa kursora
            statement.setCursorName("softwareskill");

            //Wykonanie zapytania z rezultatem
            try (var resulSet = statement.executeQuery(sqlQuery)) {

                if (resulSet.next()) {

                    resulSet.updateBigDecimal(2, BigDecimal.ZERO);

                    var isUpdated = resulSet.rowUpdated();//Jak widać nie działa dla PostgreSQL

                    //Bez tej instrukcji wiersz nie zostanie zaktualizowany
                    resulSet.updateRow(); //Nie będzie dodatkowej instrukcji UPDATE

                    isUpdated = resulSet.rowUpdated();//Jak widać nie działa dla PostgreSQL

                    System.out.println("Znaleziono kwotę do zapłaty");
                } else {
                    System.err.println("Nie znaleziono kwoty do zapłaty");
                }
            }
        }

        //Zwolnienie połączenia
        connection.close();

        //Zwolnienie puli
        HikariDatasourceConnection.close();
    }
}
