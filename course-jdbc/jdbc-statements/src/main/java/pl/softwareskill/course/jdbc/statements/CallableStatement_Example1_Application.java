package pl.softwareskill.course.jdbc.statements;

import java.sql.Types;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje wywołanie procedury createNewOrder przez JDBC dla bazy danych PostgreSQL.
 *
 * Procedura ma jeden parametr wejściowo/wyjściowy (wartość wejściowa jest nadpisywana
 * w procedurze i zwracana w wyniku).
 *
 * Logika tworzy nowe zamówienie
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class CallableStatement_Example1_Application {

    public static void main(String[] args) throws Exception {
        //Nawiązanie połączenia (PostgreSQL + Hikari, parametry w pliku datasource.properties)
        var connection = HikariDatasourceConnection.getConnection();

        //Pseudounikalna wartość numeru zamówienia
        var orderNumber = String.valueOf(System.currentTimeMillis());

        //Instrukcja dla wywołania PROCEDURY createNewOrder
        String instruction = "call public.createNewOrder(?,?,?)";

        try (var statement = connection.prepareCall(instruction)) {
            statement.setString(1, orderNumber);
            statement.setFloat(2, 123.67f);
            statement.registerOutParameter(3, Types.VARCHAR);//musi być przed ustawieniem wartości
            statement.setString(3, null); //Parametr IN OUT ustawionu na null
            statement.execute();

            //Ostatni parameytr jest INOUT więc może być do niego przypisana zwrotnie wartość w funkcji
            var status = statement.getString(3);

            System.out.println("Status dla nowego zamówienia to " + status);
        }

        //Zwolnienie połączenia
        connection.close();

        //Zwolnienie puli
        HikariDatasourceConnection.close();
    }
}
