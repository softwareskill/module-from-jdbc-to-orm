package pl.softwareskill.course.jdbc.statements;

import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Kod demonstracyjny
 *
 * Aplikacja pokazuje czasy wykonania zapytań dla połaczenia z wykorzystaniem puli połączeń Hikari
 * oraz czystego JDBC - dla bazy danych PostgreSQL.
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class StatementsExecutionTimeCompareApplication {

    public static void main(String[] args) throws Exception {
        System.out.println("Testy dla Hikari");
        var startTime = System.nanoTime();
        runTestsForHikari();
        System.out.println("Czas całkowity dla Hikari = " + (System.nanoTime() - startTime) / 1000);

        System.out.println("Testy bez Hikari");
        startTime = System.nanoTime();
        runTestsForPureJdbc();
        System.out.println("Czas całkowity bez Hikari = " + (System.nanoTime() - startTime) / 1000);
    }

    private static void runTestsForHikari() throws SQLException, IOException {
        //Nawiązanie połączenia
        var connection = HikariDatasourceConnection.getConnection();

        //Zapytanie SQL
        String sqlQuery = "SELECT AMOUNT_TO_PAY FROM ORDERS WHERE ORDER_NUMBER=?";

        runTestsForConnection(connection, sqlQuery);

        //Zwolnienie połączenia
        connection.close();

        //Zwolnienie puli
        HikariDatasourceConnection.close();
    }

    private static void runTestsForPureJdbc() throws SQLException {
        //Nawiązanie połączenia
        var connection = DriverManager.getConnection("jdbc:postgresql:softwareskill",
                "softwareskill",
                "softwareskill");

        //Zapytanie SQL
        String sqlQuery = "SELECT AMOUNT_TO_PAY FROM ORDERS WHERE ORDER_NUMBER=?";

        runTestsForConnection(connection, sqlQuery);

        //Zwolnienie połączenia
        connection.close();

        //Zwolnienie puli
        HikariDatasourceConnection.close();
    }

    private static void runTestsForConnection(java.sql.Connection connection, String sqlQuery) throws SQLException {
        var time = callWithPreparedStatementByName(connection, sqlQuery, 1);
        System.out.println("Prepared statement czas dla nazwy =" + time / 1000);

        time = callWithStatementByName(connection, sqlQuery, 1);
        System.out.println("Statement czas dla nazwy =" + time / 1000);

        time = callWithPreparedStatementByIndex(connection, sqlQuery, 1);
        System.out.println("Prepared statement czas dla indeksu =" + time / 1000);

        time = callWithStatementByIndex(connection, sqlQuery, 1);
        System.out.println("Statement czas dla indeksu =" + time / 1000);

        time = callWithPreparedStatementByIndex(connection, sqlQuery, 20);
        System.out.println("Prepared statement 10 iteracji czas dla indeksu=" + time / 1000);

        time = callWithStatementByIndex(connection, sqlQuery, 20);
        System.out.println("Statement  10 iteracji czas dla indeksu =" + time / 1000);

        time = callWithSharedStatementByIndex(connection, sqlQuery, 20);
        System.out.println("Shared statement  10 iteracji czas dla indeksu =" + time / 1000);
    }

    private static long callWithPreparedStatementByIndex(java.sql.Connection connection, String sqlQuery, int times) throws SQLException {
        var startTime = System.nanoTime();
        try (var statement = connection.prepareStatement(sqlQuery)) {

            for (int i = 0; i < times; i++) {
                //Ustawienie parametrów
                statement.setString(1, "1");

                //Wykonanie zapytania z wynikiem
                try (var result = statement.executeQuery()) {

                    //Wiemy że instrukcja to SELECT true oznacza wynik, false brak
                    if (result.next()) {
                        result.getBigDecimal(1);
                    }
                }
            }
        }
        return System.nanoTime() - startTime;
    }

    private static long callWithPreparedStatementByName(java.sql.Connection connection, String sqlQuery, int times) throws SQLException {
        var startTime = System.nanoTime();
        try (var statement = connection.prepareStatement(sqlQuery)) {
            for (int i = 0; i < times; i++) {
                //Ustawienie parametrów
                statement.setString(1, "1");

                //Wykonanie zapytania z wynikiem
                try (var result = statement.executeQuery()) {

                    //Wiemy że instrukcja to SELECT true oznacza wynik, false brak
                    if (result.next()) {
                        result.getBigDecimal("AMOUNT_TO_PAY");
                    }
                }
            }
        }
        return System.nanoTime() - startTime;
    }

    private static long callWithStatementByIndex(java.sql.Connection connection, String sqlQuery, int times) throws SQLException {
        var startTime = System.nanoTime();

        for (int i = 0; i < times; i++) {
            sqlQuery = sqlQuery.replace("?", "'1'");
            try (var statement = connection.createStatement()) {

                //Wykonanie zapytania z wynikiem
                try (var result = statement.executeQuery(sqlQuery)) {

                    //Wiemy że instrukcja to SELECT true oznacza wynik, false brak
                    if (result.next()) {
                        result.getBigDecimal(1);
                    }
                }
            }
        }
        return System.nanoTime() - startTime;
    }

    private static long callWithSharedStatementByIndex(java.sql.Connection connection, String sqlQuery, int times) throws SQLException {
        var startTime = System.nanoTime();

        try (var statement = connection.createStatement()) {
            for (int i = 0; i < times; i++) {

                var query = sqlQuery.replace("?", "'1'");

                //Wykonanie zapytania z wynikiem
                try (var result = statement.executeQuery(query)) {

                    //Wiemy że instrukcja to SELECT true oznacza wynik, false brak
                    if (result.next()) {
                        result.getBigDecimal(1);
                    }
                }
            }
        }
        return System.nanoTime() - startTime;
    }

    private static long callWithStatementByName(java.sql.Connection connection, String sqlQuery, int times) throws SQLException {
        var startTime = System.nanoTime();
        for (int i = 0; i < times; i++) {
            sqlQuery = sqlQuery.replace("?", "'1'");
            try (var statement = connection.createStatement()) {

                //Wykonanie zapytania z wynikiem
                try (var result = statement.executeQuery(sqlQuery)) {

                    //Wiemy że instrukcja to SELECT true oznacza wynik, false brak
                    if (result.next()) {
                        result.getBigDecimal("AMOUNT_TO_PAY");
                    }
                }
            }
        }
        return System.nanoTime() - startTime;
    }
}
