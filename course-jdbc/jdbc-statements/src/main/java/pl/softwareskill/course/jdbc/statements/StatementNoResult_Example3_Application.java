package pl.softwareskill.course.jdbc.statements;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje pobranie metadanych wyniku instrukcji.
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class StatementNoResult_Example3_Application {

    public static void main(String[] args) throws Exception {
        //Nawiązanie połączenia (PostgreSQL + Hikari, parametry w pliku datasource.properties)
        var connection = HikariDatasourceConnection.getConnection();

        try (var statement = connection.createStatement()) {
            //Zapytanie SQL
            String sqlQuery = "SELECT AMOUNT_TO_PAY FROM ORDERS WHERE ORDER_NUMBER='1'";

            //Wykonanie zapytania
            statement.execute(sqlQuery);

            //Pobranie wyniku
            try (var resultSet = statement.getResultSet()) {

                //Pobranie metadanych wyniku
                var metadata = resultSet.getMetaData();
                System.out.println("Liczba kolumn wyniku " + metadata.getColumnCount());

                //Dane o kolumnie 1 w wyniku
                System.out.println("Kolumna 1 - schemat " + metadata.getSchemaName(1));
                System.out.println("Kolumna 1 - nazwa tabeli " + metadata.getTableName(1));
                System.out.println("Kolumna 1 - nazwa kolumny " + metadata.getColumnName(1));
                System.out.println("Kolumna 1 - klasa kolumny " + metadata.getColumnClassName(1));
                System.out.println("Kolumna 1 - typ kolumny " + metadata.getColumnTypeName(1));
                System.out.println("Kolumna 1 - liczba miejsc przed przecinkiem/rozmiar " + metadata.getPrecision(1));
                System.out.println("Kolumna 1 - liczba miejsc  po przecinku" + metadata.getScale(1));
            }
        }

        //Zwolnienie połączenia
        connection.close();

        //Zwolnienie puli
        HikariDatasourceConnection.close();
    }
}
