package pl.softwareskill.course.jdbc.statements;

import java.sql.SQLException;
import java.sql.Statement;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje ograniczanie maksymalnej liczby wierszy pobieranych dla zapytania (maxRows)
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class StatementNoResult_Example4_Application {

    public static void main(String[] args) throws Exception {
        //Nawiązanie połączenia (PostgreSQL + Hikari, parametry w pliku datasource.properties)
        var connection = HikariDatasourceConnection.getConnection();

        try (var statement = connection.createStatement()) {
            //Zapytanie SQL
            String sqlQuery = "SELECT * FROM ORDERS";

            //Ograniczenie do 3
            doQuery(statement, sqlQuery, 3);

            //Ograniczenie do 100
            doQuery(statement, sqlQuery, 100);
        }

        //Zwolnienie połączenia
        connection.close();

        //Zwolnienie puli
        HikariDatasourceConnection.close();
    }

    private static void doQuery(Statement statement, String sqlQuery, int maxRowCount) throws SQLException {

        //Liczba wierszy do pobrania
        statement.setMaxRows(maxRowCount);

        //Timeout w sekundach
        statement.setQueryTimeout(1);

        //Wykonanie zapytania
        var status = statement.execute(sqlQuery);

        int rowCount = 0;

        //Pobranie wyniku
        try (var result = statement.getResultSet()) {

            if (status) {
                while (result.next()) {
                    rowCount++;
                }
            }
        }

        System.out.println("Liczba pobranych wierszy z ograniczeniem do " + maxRowCount + " wynosi " + rowCount);
    }
}
