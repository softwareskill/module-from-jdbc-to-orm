package pl.softwareskill.course.jdbc.statements;

import java.sql.DriverManager;
import static org.postgresql.core.QueryExecutor.QUERY_FORWARD_CURSOR;
import org.postgresql.jdbc.PgStatement;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje ograniczenie liczby pobieranych wierszy dla odczytywania partiami przez JDBC dla zapytania (fetchSize)
 * dla sterownika JDBC bazy PostgreSQL
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class StatementFetchSizeExampleApplication {

    public static void main(String[] args) throws Exception {

        //Nie jest konieczne bo jest plik META-INF/services/java.sqlDriver w jar ze sterownikiem
        String driverclassName = "org.postgresql.Driver";
        Class.forName(driverclassName);

        //Nawiązanie połączenia - sterownik PostgreSQL
        var connection = DriverManager.getConnection("jdbc:postgresql:softwareskill", "softwareskill", "softwareskill");

        connection.setAutoCommit(false);

        try (var statement = (PgStatement) connection.createStatement()) {
            //Zapytanie SQL
            String sqlQuery = "SELECT * FROM ORDERS";

            //Ustawienie rozmiaru paczki
            statement.setFetchSize(3);

            //Wykonanie zapytania
            var status = statement.executeWithFlags(sqlQuery, QUERY_FORWARD_CURSOR);

            int rowCount = 0;

            //Pobranie wyniku - zostaną pobrane od razu wszystkie wyniki
            try (var result = statement.getResultSet()) {

                if (status) {
                    while (result.next()) {
                        rowCount++;
                    }
                }
            }
            System.out.println("Odczytano " + rowCount + " wierszy");
        }

        //Zwolnienie połączenia
        connection.close();
    }
}
